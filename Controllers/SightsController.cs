﻿using Microsoft.AspNetCore.Mvc;

namespace Travel2.Controllers
{
    public class SightsController : Controller
    {
        public IActionResult Arstanbob()
        {
            return View();
        }
        public IActionResult SaimaluuTash()
        {
            return View();
        }
         public IActionResult Batken()
        {
            return View();
        }
        public IActionResult Jalalabad()
        {
            return View();
        }
        public IActionResult Issykkol()
        {
            return View();
        }
        public IActionResult Songkol()
        {
            return View();
        }
        public IActionResult Tashrabat()
        {
            return View();
        }
        public IActionResult Osh()
        {
            return View();
        }
        public IActionResult Kelsuu()
        {
            return View();
        }
        public IActionResult Bishkek()
        {
            return View();
        }
        public IActionResult Talas()
        {
            return View();
        }
        public IActionResult Sarychelek()
        {
            return View();
        }
    }
}
