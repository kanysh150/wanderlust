﻿using Microsoft.AspNetCore.Mvc;

namespace Travel2.Controllers
{
    public class TourController : Controller
    {
        public IActionResult Biketour1()
        {
            return View();
        }
        public IActionResult CentralAsia1()
        {
            return View();
        }
        public IActionResult Trekking1()
        {
            return View();
        }
        public IActionResult OneDayTour3()
        {
            return View();
        }
        public IActionResult OneDayTour5()
        {
            return View();
        }
        public IActionResult HorsebackTour1()
        {
            return View();
        }
        public IActionResult OneDayTour1()
        {
            return View();
        }
        public IActionResult OneDayTour2()
        {
            return View();
        }
        public IActionResult OneDayTour4()
        {
            return View();
        }
        public IActionResult OneDayTour6()
        {
            return View();
        }
        public IActionResult Trekking2()
        {
            return View();
        }
        public IActionResult Trekking3()
        {
            return View();
        }
    }
}
