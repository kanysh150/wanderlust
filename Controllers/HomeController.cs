﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using Travel2.Models;

namespace Travel2.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult AboutKyrgyzstan()
        {
            return View();
        }
        public IActionResult BikeTours()
        {
            return View();
        }
        public IActionResult CentralAsiaTours()
        {
            return View();
        }
        public IActionResult HorsebackTours()
        {
            return View();
        }
        public IActionResult OneDayTours()
        {
            return View();
        }
        public IActionResult Trekking()
        {
            return View();
        }
        public IActionResult Sights()
        {
            return View();
        }
        public IActionResult Blog()
        {
            return View();
        }
        public IActionResult PhotoGallery()
        {
            return View();
        }
        
       
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}